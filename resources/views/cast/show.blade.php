@extends('layouts.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4> {{ $cast->nama }} , {{ $cast->umur }} </h4>
        <p> {{ $cast->bio }} </p>
    </div>
@endsection