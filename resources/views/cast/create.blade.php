@extends('layouts.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '')}}" placeholder="Enter Nama">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', '')}}" placeholder="Enter Umur">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" id="bio" name="bio" value="{{ old('bio', '')}}" placeholder="Enter Bio" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
</div>
@endsection