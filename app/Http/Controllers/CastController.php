<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create() {
        return view('cast.create');
    }

    public function store(request $request) {
        // dd($request->all());

        $request->validate([
            'nama' => 'required|unique:cast|max:255',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Cast berhasil disimpan');
    }

    public function index() {
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id) {
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($cast);
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id) {
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($cast);
        return view('cast.edit', compact('cast'));
    }

    public function update($cast_id, request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
                    ->where('id', $cast_id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);
        return redirect('/cast')->with('success', 'Cast berhasil diperbarui');
    }

    public function destroy($cast_id) {
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success', 'Cast berhasil dihapus');
    }
}
